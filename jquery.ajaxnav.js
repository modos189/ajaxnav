/**
 * AjaxNav Jquery plugin v1.1.0
 * https://bitbucket.org/MrBoriska/ajaxnav/overview
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
*/

;(function ($) {
    $.fn.AjaxNav = function(s) {
        
        this.AjaxNav.options = $.extend({
            loadTo: $("#site-inner"),
            findTo: "a",
            ignoreWith: [],
            ignoreIn: [],
            onLoad: function() {},
            onCheckLink: function() {},
            onReload: function(url, from_history) {},
            loadMethod: 'GET',
            loadWithCache: false,
            loadWithData: {ajax: true},
            showProgress: function() {},
            hideProgress: function() {},
            navCacheprefix: 'NavigationCache-'
        }, $.fn.AjaxNav.defaults, s);
        
        // Если локальное WEB хранилище не поддерживается браузером,
        // то работаем как с обычной переменной-массивом.(исчезнет после перезагрузки страницы)
        if(typeof(Storage)=="undefined"){
            window.sessionStorage = [];
        }
        // first page
        var page_key = this.AjaxNav.options.navCacheprefix+window.location.pathname;
        if (typeof(sessionStorage[page_key]) !== "undefined" &&
            window.sessionStorage[page_key] !== this.AjaxNav.options.loadTo.html()
        ) {
            window.sessionStorage[page_key] = this.AjaxNav.options.loadTo.html();
            history.replaceState({page: window.location.pathname, type: "page"}, document.title, window.location.pathname);
        }
        
        var self = this;
        window.addEventListener('popstate', function(event){
            if (event.state &&
                event.state.type &&
                event.state.type.length > 0 &&
                window.sessionStorage[self.AjaxNav.options.navCacheprefix+event.state.page] &&
                window.sessionStorage[self.AjaxNav.options.navCacheprefix+event.state.page].length > 0
             ) {
                self.AjaxNav_setPage.apply(self, [sessionStorage[self.AjaxNav.options.navCacheprefix+event.state.page], event.state.page, true])
            }
        }, false);
        
        function actionClick() {
            
            var link = $(this),
                href = link.attr('href');
            
            var ignore = false;
            // ignoreWith
            for (var i in self.AjaxNav.options.ignoreWith)
                if (link.is(self.AjaxNav.options.ignoreWith[i])) {
                    ignore = true;
                    break;
                }
            
            // ignoreIn
            if (!ignore)
                for (var i in self.AjaxNav.options.ignoreIn)
                    if (link.closest(self.AjaxNav.options.ignoreIn[i], self)[0]) {
                        ignore = true;
                        break;
                    }
            
            if (
                !ignore &&
                href &&
                link.attr("target") !== "_blank" && // Внешние ссылки не трогать
                href !== '#' && // Ссылки-якори не трогать
                href.indexOf('javascript:') == -1 &&// Дурные ссылки тоже не трогать...
                (typeof((onCheckLink = self.AjaxNav.options.onCheckLink.apply(self))) == "undefined"
                || onCheckLink == true) // Дополнительная фильтрация
            ) {

                self.AjaxNav_loadPage.apply(self,[href]);

                return false;
            } else
                return true; // перейти по адресу, как обычно.
        }
        this.on("click", this.AjaxNav.options.findTo, actionClick);
    };
    $.fn.AjaxNav.defaults = {};
    $.fn.AjaxNav_loadPage = function (page) {
        this.AjaxNav.options.showProgress();
        var self = this;

        $.ajax({
            cache: this.AjaxNav.options.loadWithCache,
            type: this.AjaxNav.options.loadMethod,
            url: page,
            data: this.AjaxNav.options.loadWithData,
            success: function(e) {
                if (self.AjaxNav.options.onLoad.apply(this, [e]) !== false) {
                    // Запоминаем текущую страницу в историю
                    window.sessionStorage[self.AjaxNav.options.navCacheprefix + page] = e;
                    history.pushState({page: page, type: "page"}, document.title, page);
                    self.AjaxNav_setPage.apply(self, [e, page, false]);

                    self.AjaxNav.options.hideProgress();
                }
            }
        });
    };
    $.fn.AjaxNav_setPage = function (data, page, from_history) {
        // Выводим на страницу данные
        this.AjaxNav.options.loadTo.html(data);
        var self = this;
        this.AjaxNav.options.loadTo.promise().done(function () {
            // Обрабатываем содержимое измененной страницы
            self.AjaxNav.options.onReload(page, from_history)

            self.AjaxNav.options.hideProgress();
        });
    };
}(jQuery));
