## Ajax Navigation Jquery plugin

### Использование:
Плагин позволяющий создать на вашем сайте ajax навигацию, просто и легко. Плагин работает таким образом, что блокирует обычную загрузку при клике по ссылкам, заменяя её ajax загрузкой в указанный элемент DOM с изменением адреса страницы так, как будто бы вы перешли на неё обычным образом. Используя эту библиотеку можно практически на любой CMS организовать ajax загрузку страниц путем изменения только кода шаблона.

Пример использования в Atom-M CMS:

```
#!html

{% if ReadGET('ajax') == false %}
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Script-Type" content="text/javascript">
        
        <title>{{ title }}</title>
        <meta name="description" content="{{ meta_description }}" />
        <meta name="keywords" content="{{ meta_keywords }}" />
    </head>
    <body>
        <div id="site-inner">
{% endif %}

            <div>main content</div>
            <a href="/path">Я загружусь ajax ом</a>
            <a href="/path" class="noajax">А меня проигнорируют :(</a>
            <div class="no_ajax_block">
                <a href="/path">А меня проигнорируют из-за родителя... :(</a>
                <span>
                    <a href="/path">И никто меня не спасет... :(</a>
                </span>
            </div>

{% if ReadGET('ajax') == false %}
        </div>
        
        <script src="jquery.min.js"></script>
        <script src="jquery.ajaxnav.min.js"/></script>
        <script>
            $(document).ready(function() {
                $("body").AjaxNav({
                    ignoreWith: [".noajax"], // or $(".noajax")
                    ignoreIn: [".no_ajax_block"] // or $(".no_ajax_block")
                    
                    /* Default options:
                    loadTo: $("#site-inner"),
                    findTo: "a",
                    ignoreWith: [],
                    ignoreIn: [],
                    onLoad: function() {},
                    onCheckLink: function() {},
                    onReload: function(url, from_history) {},
                    loadMethod: 'GET',
                    loadWithCache: false,
                    loadWithData: {ajax: true},
                    showProgress: function() {},
                    hideProgress: function() {},
                    navCacheprefix: 'NavigationCache-'
                    */
                })
            });
        </script>
    </body>
</html>

{% else %}
    <script>
        document.title = "{{ addslashes(title) }}";
        $('meta[name=description]').attr('content', "{{ addslashes(meta_description) }}");
        $('meta[name=keywords]').attr('content', "{{ addslashes(meta_keywords) }}");
    </script>
{% endif %}

```




### Лицензия:

Лицензия свободная, MIT
http://www.opensource.org/licenses/mit-license.php

Удачи в использовании) Приму пожелания и замечания в задачах к репозиторию.